create table owner_repository_data
(
	id bigint auto_increment
		primary key,
	login varchar(255) null,
	owner_id bigint null,
	site_admin bit not null
)
;

create table git_repo_data
(
	id bigint auto_increment
		primary key,
	description varchar(255) null,
	error varchar(255) null,
	name varchar(255) null,
	repo_id bigint null,
	url varchar(255) null,
	watchers_count bigint null,
	owner_id bigint null,
	constraint FKqtwb2vwprv2yx1qfwmqv8jf1n
		foreign key (owner_id) references owner_repository_data (id)
)
;

create index FKqtwb2vwprv2yx1qfwmqv8jf1n
	on git_repo_data (owner_id)
;

create table personal_data
(
	id bigint not null auto_increment
		primary key,
	email varchar(255) null,
	name varchar(255) null
)
;

create table commit_detail
(
	id bigint auto_increment
		primary key,
	message varchar(255) null,
	author_id bigint null,
	committer_id bigint null,
	constraint FKqas51fy6pm9bs89tvloq60e18
		foreign key (author_id) references personal_data (id),
	constraint FKqbwu4jgkl18svbk1ab2tskxyf
		foreign key (committer_id) references personal_data (id)
)
;

create index FKqas51fy6pm9bs89tvloq60e18
	on commit_detail (author_id)
;

create index FKqbwu4jgkl18svbk1ab2tskxyf
	on commit_detail (committer_id)
;

create table commit_general_data
(
	id bigint auto_increment
		primary key,
	url varchar(255) null,
	commit_detail_id bigint null,
	constraint FKeld7vb0fpejq03frkm5uuf0l3
		foreign key (commit_detail_id) references commit_detail (id)
)
;

create index FKeld7vb0fpejq03frkm5uuf0l3
	on commit_general_data (commit_detail_id)
;

