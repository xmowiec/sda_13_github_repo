package pl.sda.course.SDA_13_GitHub_repo.errorHandling;

public class SDAException extends RuntimeException {

    public SDAException(String message) {
        super(message); //call RuntimeException constructor with given message
    }
}
