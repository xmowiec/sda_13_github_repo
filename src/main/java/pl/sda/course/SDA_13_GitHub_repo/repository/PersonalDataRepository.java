package pl.sda.course.SDA_13_GitHub_repo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.course.SDA_13_GitHub_repo.domain.PersonalData;

/**
 * Repository interface - no methods signature nor implement, use only inherited methods.
 */
@Repository
public interface PersonalDataRepository extends JpaRepository<PersonalData, Long> {

}
