package pl.sda.course.SDA_13_GitHub_repo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.web.client.RestTemplate;

/**
 * Created with <a href="http://start.spring.io/" target="_top">Spring Initializr</a>.
 * Use <b>Spring Boot</b> to run and configure whole application
 * Important: Convention Over Configuration
 */
@SpringBootApplication
@EnableKafka
public class Sda13GitHubRepoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sda13GitHubRepoApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
