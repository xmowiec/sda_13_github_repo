package pl.sda.course.SDA_13_GitHub_repo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.sda.course.SDA_13_GitHub_repo.domain.CommitGeneralData;
import pl.sda.course.SDA_13_GitHub_repo.domain.GitRepoData;
import pl.sda.course.SDA_13_GitHub_repo.domain.OwnerRepositoryData;
import pl.sda.course.SDA_13_GitHub_repo.errorHandling.SDAException;
import pl.sda.course.SDA_13_GitHub_repo.repository.GitRepoDataRepository;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GitRepoServiceTest {

    private static final String GIT_API_URL = "https://api.github.com/repos/{userName}/{repositoryName}";
    private static final String GIT_API_URL_COMMITS = GIT_API_URL + "/commits";
    private static final String ERROR_MESSAGE = "test_error";
    private static final String TEST_USER = "test_user";
    private static final String TEST_REPO = "test_repo";
    private static final String FULL_NAME = TEST_USER + "/" + TEST_REPO;
    private static final String TEST_URL_0 = "test_URL_0";
    private static final String TEST_URL_1 = "test_URL_1";

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private GitRepoDataRepository gitRepoDataRepository;
    @InjectMocks
    private GitRepoService gitRepoService;

    @Test
    public void shouldReturnValidResponseForQuery() {
        //given - most used in groovy

        OwnerRepositoryData ownerData = new OwnerRepositoryData();
        ownerData.setLogin("test_login");
        ownerData.setSiteAdmin(false);

        GitRepoData gitRepoData = new GitRepoData();
        gitRepoData.setName("test_name");
        gitRepoData.setDescription("test_description");
        gitRepoData.setOwner(ownerData);

        when(restTemplate.getForObject(any(String.class), eq(GitRepoData.class),
                any(String.class), any(String.class))).thenReturn(gitRepoData);
        when((gitRepoDataRepository.existsByName(FULL_NAME))).thenReturn(false);

        //when
        GitRepoData underTest = gitRepoService.getRepositoryByUserAndRepoName(TEST_USER, TEST_REPO);

        //then
        assertThat(underTest.getName()).isEqualTo(gitRepoData.getName());
    }

    @Test
    public void shouldGetErrorWhen4xxFromGithub() {
        // given
        when(restTemplate.getForObject(GIT_API_URL, GitRepoData.class, TEST_USER, TEST_REPO))
                .thenThrow(new HttpClientErrorException(HttpStatus.FORBIDDEN, ERROR_MESSAGE));

        //when
        GitRepoData underTest = gitRepoService.getRepositoryByUserAndRepoName(TEST_USER, TEST_REPO);

        //then
        assertThat(underTest.getError()).isEqualTo(HttpStatus.FORBIDDEN.value() + " " + ERROR_MESSAGE);
    }

    @Test
    public void shouldReturnValidListOfCommits() {

        //given
        CommitGeneralData[] commitGeneralDataArray = new CommitGeneralData[3];
        commitGeneralDataArray[0] = new CommitGeneralData();
        commitGeneralDataArray[1] = new CommitGeneralData();
        commitGeneralDataArray[2] = new CommitGeneralData();
        commitGeneralDataArray[0].setUrl(TEST_URL_0);
        commitGeneralDataArray[1].setUrl(TEST_URL_1);

        when(restTemplate.getForObject(GIT_API_URL_COMMITS, CommitGeneralData[].class,
                TEST_USER, TEST_REPO)).thenReturn(commitGeneralDataArray);

        //when
        List<CommitGeneralData> commitsListFromMock = gitRepoService
                .getCommitsListByUserAndRepoName(TEST_USER, TEST_REPO);

        //then
        assertThat(commitsListFromMock.size()).isEqualTo(commitGeneralDataArray.length);
        assertThat(commitsListFromMock.stream()
                .map(CommitGeneralData::getUrl)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()))
                .containsExactlyInAnyOrder(TEST_URL_0, TEST_URL_1);
    }

    @Test(expected = SDAException.class)
    public void shouldThrowSDAException() {
        //given
        //it's very important to pass the right class type as the parameter with square brackets
        //IDE doesn't mark this as an error

        when(restTemplate.getForObject(GIT_API_URL_COMMITS, CommitGeneralData[].class, TEST_USER, TEST_REPO))
                .thenThrow(new HttpClientErrorException(HttpStatus.FORBIDDEN, ERROR_MESSAGE));
        //when
        gitRepoService.getCommitsListByUserAndRepoName(TEST_USER, TEST_REPO);

        //then
    }
}