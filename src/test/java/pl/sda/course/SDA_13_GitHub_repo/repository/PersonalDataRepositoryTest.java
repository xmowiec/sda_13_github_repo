package pl.sda.course.SDA_13_GitHub_repo.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.sda.course.SDA_13_GitHub_repo.domain.PersonalData;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonalDataRepositoryTest {

    @Autowired
    private PersonalDataRepository personalDataRepository;

    @Test
    public void shouldAddDataToRepository(){
        //given
        PersonalData person = new PersonalData();
        person.setName("name");
        person.setEmail("email@gmail.com");

        //when
        personalDataRepository.save(person); // save method from CrudRepository

        //then
        // result for query into database (implements in SimpleJpaRepository class)
        List<PersonalData> personListFromDB = personalDataRepository.findAll();
        // System.out.println(personListFromDB);
        PersonalData personFromDB = personListFromDB.get(0);
        assertThat(personListFromDB.size()).isEqualTo(1); // ensure findAll return exact one item
        assertThat(personFromDB.getName()).isEqualTo(person.getName());
        assertThat(personFromDB.getEmail()).isEqualTo(person.getEmail());
        // since id has no setter (disabled from lombok), the only way to obtain value is generate it by db
        assertThat(personFromDB.getId()).isNotNull(); // ensure db gives id to this record
    }
}